package com.hcl.ppmtoolapi.serviceimpl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.ppmtoolapi.domain.Backlog;
import com.hcl.ppmtoolapi.domain.ProjectTask;
import com.hcl.ppmtoolapi.exception.ProjectNotFoundException;
import com.hcl.ppmtoolapi.repository.BackLogRepository;
import com.hcl.ppmtoolapi.repository.ProjectTaskRepository;
import com.hcl.ppmtoolapi.service.ProjectTaskService;

@Service
public class ProjectTaskServiceImpl implements ProjectTaskService {
	@Autowired
	private BackLogRepository backlogRepository;
	
	@Autowired
	private ProjectTaskRepository projectTaskRepositaory;
	
	
	@Override
	public ProjectTask addProjectTask(String projectIdentifier, ProjectTask projectTask) {
		
	try {
		
		Backlog backlog=	backlogRepository.findByProjectIdentifier(projectIdentifier.toUpperCase() );
		
			
			projectTask.setBacklog(backlog);
			
			
			Integer backlogSequnece= backlog.getPTsequence();
		
		
		backlogSequnece++;
		backlog.setPTsequence(backlogSequnece);
		
		
		projectTask.setProjectTaskSequence(projectIdentifier + "-" + backlogSequnece);
		projectTask.setProjectIdentifier(projectIdentifier);
		
		
			if(projectTask.getPriority()==null) {
				projectTask.setPriority(3);
			}
			
		
			if(projectTask.getStatus()==null) {
				projectTask.setStatus("ToDo");
			}
			return projectTaskRepositaory.save(projectTask);
	}
	catch(Exception ex){
		throw new ProjectNotFoundException("Project Not Found");
	}
	}


	@Override
	public ProjectTask findPTByProjectSequence(String backlog_id, String projectTaskSequence) {
		// make sure that we are searching under existing backlog_id
	Backlog backlog=	backlogRepository.findByProjectIdentifier(backlog_id);
	if(backlog==null) {
		throw new ProjectNotFoundException("project with id  :"+backlog_id+"it does not exist");
	}
   ProjectTask projectTask=	projectTaskRepositaory.findByProjectTaskSequence(projectTaskSequence);
   if(projectTask==null) {
	   throw new ProjectNotFoundException("project Task with id  :"+projectTaskSequence+"it does not exist");
	   
   }
		return projectTask;
	}


	@Override
	public void deletePTBysequence(String backlog_id, String pt_id) {
	ProjectTask projectTask=	findPTByProjectSequence(backlog_id, pt_id);
	Backlog backlog= projectTask.getBacklog();
	List<ProjectTask> projectTasks=	backlog.getProjectTask();
		projectTasks.remove(projectTask);
		backlogRepository.save(backlog);
		projectTaskRepositaory.delete(projectTask);
		
	}

}
