package com.hcl.ppmtoolapi.serviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.ppmtoolapi.domain.Backlog;
import com.hcl.ppmtoolapi.domain.Project;
import com.hcl.ppmtoolapi.exception.ProjectIdException;
import com.hcl.ppmtoolapi.repository.BackLogRepository;
import com.hcl.ppmtoolapi.repository.ProjectRepository;
import com.hcl.ppmtoolapi.service.ProjectService;

@Service
public class ProjectServiceImpl implements ProjectService {
	@Autowired
	private ProjectRepository projectRepository;
	
		@Autowired
	private BackLogRepository backlogRepository;
		
	@Override
	public Project saveOrUpdate(Project project) {
		
		try {
			project.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());
			// BIZ logic
			//when project is created backlog should create
		if(project.getId()== null) {
			Backlog backlog=new Backlog();
			project.setBacklog(backlog);
			backlog.setProject(project);
			backlog.setProjectIdentifier(project.getProjectIdentifier().toUpperCase());
		}
		// when project is updated backlog should not be null
		
	if(project.getId()!=null) {
			project.setBacklog(backlogRepository.findByProjectIdentifier(project.getProjectIdentifier().toUpperCase()));
		}
			
	
		
		return projectRepository.save(project );
		}catch (Exception ex){
			 throw new ProjectIdException("ProjectID :"+project.getProjectIdentifier()+" already exists");
			
		}
	}

	@Override
	public Project findProjectByProjectIdentifer(String projectId) {
	Project project=	projectRepository.findByProjectIdentifier(projectId.toUpperCase());
	if(project==null) {
		throw new ProjectIdException("Project identifer :"+projectId.toUpperCase()+" Does Not Exists");
	}
		return project;
	}

	@Override
	public Iterable<Project> findAllproject() {
		// TODO Auto-generated method stub
		return projectRepository.findAll();
	}

	@Override
	public void deleteProjectByIdentifier(String projectId) {
		Project project=	projectRepository.findByProjectIdentifier(projectId.toUpperCase());
		if(project==null) {
			throw new ProjectIdException("Project identifer :"+projectId.toUpperCase()+" Does Not Exists");
		}
		projectRepository.delete(project);
	
	}
		
}
