package com.hcl.ppmtoolapi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.ppmtoolapi.domain.Project;

@Repository
public interface ProjectRepository extends CrudRepository<Project, Long>  {
	//TODO: No need to add CRUD operation here, if any customization required , we can customize CRUD method
		Project findByProjectIdentifier(String projectIdentifier);
		Iterable<Project> findAll();
		
		
}
