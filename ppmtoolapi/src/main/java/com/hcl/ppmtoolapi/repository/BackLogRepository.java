package com.hcl.ppmtoolapi.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hcl.ppmtoolapi.domain.Backlog;

@Repository
public interface BackLogRepository extends CrudRepository<Backlog, Long> {
		Backlog findByProjectIdentifier(String projectIdentifier);
}
