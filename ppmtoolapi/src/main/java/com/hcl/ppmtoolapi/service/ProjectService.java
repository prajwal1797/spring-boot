package com.hcl.ppmtoolapi.service;

import com.hcl.ppmtoolapi.domain.Project;

public interface ProjectService {
	
	public  Project saveOrUpdate(Project project);
	public Project findProjectByProjectIdentifer(String projectId);
	public Iterable<Project> findAllproject();
	public void deleteProjectByIdentifier(String projectId);
}
