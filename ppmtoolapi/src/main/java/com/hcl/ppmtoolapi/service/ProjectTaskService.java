package com.hcl.ppmtoolapi.service;


import com.hcl.ppmtoolapi.domain.ProjectTask;

public interface ProjectTaskService {
		
	public ProjectTask addProjectTask(String projectIdentifier, ProjectTask projectTask);
	ProjectTask findPTByProjectSequence(String backlog_id, String projectTaskSequence);
	public void deletePTBysequence(String backlog_id,String pt_id);
}
